## Overview

Code for this project was developed in two separate source repositories: one in Python, and one in Matlab.

* data analysis: [temporal-integration-python](https://bitbucket.org/huklab/temporal-integration-python)
* figure generation: [temporal-integration-matlab](https://bitbucket.org/huklab/temporal-integration-matlab)

## Generating figures

### Requirements

To generate figures from the paper you will need to install the python dependencies listed in `temporal-integration-python/requirements.txt`.
Psignifit, which fits psychometric functions using Bayesian inference, is easiest to install on Ubuntu. Follow the instructions [here](http://neuro.debian.net/install_pkg.html?p=python-pypsignifit) to install Psignifit using neuro-debian. (Coincidentally, this will install all the other required packages as well!)

### Instructions

0. In command line, set `OUTDIR=temporal-integration-matlab/fits`.
1. Run `temporal-integration-python/bin/fig_data_gen.sh $OUTDIR` to generate fits. (This will take a while.)
2. In Matlab, run `temporal-integration-matlab/bin/main.m`.

The resulting pdf figures should now be in `temporal-integration-matlab/plots`.

![stimulus-fig](raw/master/stimulus-fig.png)